/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_min_max.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/26 13:20:09 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 13:24:25 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	*ft_min_max(t_map *m)
{
	int			i;
	int			j;
	static int	m_m[2];

	m_m[0] = m->mass[0][0];
	m_m[0] = m_m[1];
	j = 0;
	while (j < m->rows)
	{
		i = 0;
		while (i < m->column)
		{
			if (m_m[1] < m->mass[j][i])
				m_m[1] = m->mass[j][i];
			if (m_m[0] > m->mass[j][i])
				m_m[0] = m->mass[j][i];
			i++;
		}
		j++;
	}
	return (m_m);
}
