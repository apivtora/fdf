/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 10:51:14 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 10:14:20 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_nbr(char nbr, int base)
{
	char	*list;
	char	*list2;
	int		i;

	list = "0123456789abcdef";
	list2 = "0123456789ABCDEF";
	i = 0;
	while (i < base)
	{
		if (list[i] == nbr || list2[i] == nbr)
		{
			return (i);
		}
		i++;
	}
	return (0);
}

int			ft_atoi_base(const char *str, int base)
{
	int i;
	int n;
	int neg;

	n = 0;
	i = 0;
	neg = 1;
	while (str[i] == ' ' || (str[i] >= '\t' && str[i] <= 14))
		i++;
	if (str[i] == '-' && str[i + 1] >= '0' && str[i + 1] <= '9' && base == 10)
	{
		i++;
		neg = -1;
	}
	if (str[i] == '+')
		i++;
	while ((str[i] >= '0' && str[i] <= '9') ||
			(str[i] >= 'a' && str[i] <= 'f') ||
			(str[i] >= 'A' && str[i] <= 'F'))
	{
		n = base * n + ft_nbr(str[i], base);
		i++;
	}
	return (n * neg);
}
