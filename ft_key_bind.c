/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_bind.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/26 10:49:28 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 13:23:27 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#define MLX mlx_string_put

void	ft_key_bind(int keycode, t_map *m)
{
	static int bind = 1;

	if (keycode == 9)
		bind = -bind;
	if (bind > 0)
	{
		MLX(m->mlx, m->win, 550, 20, 0x00FF8C00, "Key bindings");
		MLX(m->mlx, m->win, 150, 40, 0x00FF8C00, "Rotation:");
		MLX(m->mlx, m->win, 50, 70, 0x00FF8C00, "X-axis - [Num 8]/[Num 5]");
		MLX(m->mlx, m->win, 50, 90, 0x00FF8C00, "Y-axis - [Num 4]/[Num 6]");
		MLX(m->mlx, m->win, 50, 110, 0x00FF8C00, "Z-axis - [Num 7]/[Num 9]");
		MLX(m->mlx, m->win, 480, 40, 0x00FF8C00, "Movement:");
		MLX(m->mlx, m->win, 350, 70, 0x00FF8C00,
				"Up/Down - [Arrow UP] / [Arrow DOWN]");
		MLX(m->mlx, m->win, 350, 90, 0x00FF8C00,
				"Left/Right - [[Arrow LEFT] / [Arrow RIGHT]");
		MLX(m->mlx, m->win, 350, 110, 0x00FF8C00,
				"Zoom in/out - [Num +] / [Num -]");
		MLX(m->mlx, m->win, 930, 40, 0x00FF8C00, "Others:");
		MLX(m->mlx, m->win, 800, 60, 0x00FF8C00, "Altitude -/+ - [Z]/[A]");
		MLX(m->mlx, m->win, 800, 80, 0x00FF8C00, "Color/alt. ON/OFF - [C]");
		MLX(m->mlx, m->win, 800, 100, 0x00FF8C00, "Hide/show controls - [V]");
		MLX(m->mlx, m->win, 800, 120, 0x00FF8C00, "X-grid ON/OFF - [X]");
		MLX(m->mlx, m->win, 800, 140, 0x00FF8C00, "Reset all - [space]");
	}
}
