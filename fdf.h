/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 16:10:37 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 15:03:44 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <mlx.h>
# include <stdlib.h>
# include <math.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <string.h>
# include "get_next_line.h"
# define PI 3.14159265359
# define COF (PI / 180)

typedef struct	s_color
{
	int			r;
	int			g;
	int			b;
}				t_color;

typedef struct	s_line
{
	int			x0;
	int			y0;
	int			x1;
	int			y1;
	int			dx;
	int			sx;
	int			dy;
	int			sy;
	int			err;
	int			e2;
}				t_line;

typedef struct	s_map2d
{
	int			x;
	int			y;
	int			c;
	int			alt_c;
}				t_map2d;

typedef struct	s_map
{
	int			column;
	int			rows;
	void		*mlx;
	void		*win;
	int			**mass;
	int			k_x;
	int			k_y;
	float		k_z;
	int			c_x;
	int			c_y;
	int			shift_x;
	int			shift_y;
	float		sc;
	t_map2d		***m2d;
}				t_map;

void			ft_change_color(t_map *m);
int				ft_atoi_base(const char *str, int base);
t_map			*ft_m_create();
void			ft_line(t_map2d *first, t_map2d *second, t_map *m);
int				**ft_read(char *argv, t_map *m);
void			ft_build(t_map *m);
t_map2d			***ft_map2d_create(t_map *map);
void			ft_map2d_fill(t_map *m, int o_x, int o_y, int o_z);
void			ft_color_switch(t_map *m);
void			ft_build_x(t_map *m);
void			ft_key_bind(int keycode, t_map *m);
int				*ft_min_max(t_map *m);
void			ft_make_z(t_map *m);
void			ft_check(char *line, t_map *m);
void			ft_make_k(int first, int second, t_color *k_col);
void			ft_error1(int fd);
int				ft_make_col(char *temp);
void			ft_rotate(int keycode, int *xyz);
void			ft_move(int keycode, t_map *m);
void			ft_zoom_z(int keycode, t_map *m);
void			ft_reset(int keycode, t_map *m);
#endif
