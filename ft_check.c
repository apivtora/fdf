/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/26 15:59:30 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 13:26:49 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_check(char *line, t_map *m)
{
	int i;
	int c2;

	i = 0;
	c2 = 0;
	while (line[i])
	{
		if (line[i] >= '0' && line[i] <= '9')
		{
			c2++;
			while (line[i + 1] != ' ' && line[i + 1])
				i++;
		}
		i++;
	}
	if (c2 != m->column)
	{
		write(1, "\x1b[31m", 5);
		write(1, "Error: ", 7);
		write(1, "\x1b[0m", 4);
		write(1, "wrong map!\n", 11);
		exit(0);
	}
}
