/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_make_k.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 10:52:03 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 13:27:31 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_make_k(int first, int second, t_color *k_col)
{
	k_col->r = (first >> 16) - (second >> 16);
	k_col->g = ((first >> 8) & 0xff) - ((second >> 8) & 0xff);
	k_col->b = (first & 0xff) - (second & 0xff);
}
