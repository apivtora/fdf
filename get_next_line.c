/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/05 12:09:05 by apivtora          #+#    #+#             */
/*   Updated: 2017/01/10 17:31:48 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static t_chain	*ft_node(int fd)
{
	static t_chain	*first_node = NULL;
	t_chain			*node;
	t_chain			*new_node;

	if (!first_node)
	{
		first_node = malloc(sizeof(t_chain));
		first_node->new_fd = fd;
		first_node->next = NULL;
		return (first_node);
	}
	node = first_node;
	while (node->new_fd != fd)
	{
		if (!(node->next))
		{
			new_node = malloc(sizeof(t_chain));
			new_node->new_fd = fd;
			new_node->next = NULL;
			node->next = new_node;
			return (new_node);
		}
		node = node->next;
	}
	return (node);
}

static void		ft_del_line(char (*buf)[BUFF_SIZE], int i)
{
	int k;

	k = 0;
	while (i + k < BUFF_SIZE)
	{
		(*buf)[k] = (*buf)[i + k];
		k++;
	}
	while (k < BUFF_SIZE)
	{
		(*buf)[k] = '\0';
		k++;
	}
}

static int		ft_read_body(t_chain *node, long flags[2], char **str, int (*i))
{
	char	*new_str;
	long	j;

	if (flags[0] < 0)
		return (-1);
	j = 0;
	new_str = ft_memalloc(sizeof(*new_str) * (flags[0] + flags[1] * BUFF_SIZE));
	ft_strcpy(new_str, (*str));
	free((*str));
	(*str) = new_str;
	ft_bzero((node->buf + flags[0]), BUFF_SIZE - flags[0]);
	while (node->buf[j] != '\n' && j < flags[0])
		(*str)[(*i)++] = node->buf[j++];
	(*str)[(*i)] = '\0';
	if (node->buf[j] == '\n')
	{
		ft_del_line(&node->buf, j + 1);
		return (1);
	}
	return (0);
}

static int		ft_read(t_chain *node, char *str, int i, char **line)
{
	long	flags[2];
	char	new_flag;

	flags[1] = 1;
	while ((flags[0] = read(node->new_fd, node->buf, BUFF_SIZE)))
	{
		new_flag = ft_read_body(node, flags, &str, &i);
		if (new_flag == -1)
			return (-1);
		if (new_flag == 1)
		{
			(*line) = str;
			return (1);
		}
		flags[1]++;
	}
	if (node->buf[0])
	{
		(*line) = str;
		ft_del_line(&node->buf, BUFF_SIZE);
		return (1);
	}
	(*line) = NULL;
	return (0);
}

int				get_next_line(const int fd, char **line)
{
	t_chain	*node;
	char	*str;
	int		i;

	i = 0;
	if (fd < 0)
		return (-1);
	node = ft_node(fd);
	while (node->buf[i])
		i++;
	str = ft_memalloc(i + 1);
	i = 0;
	while (node->buf[i] != '\n' && node->buf[i])
	{
		str[i] = node->buf[i];
		i++;
	}
	if (node->buf[i] == '\n')
	{
		ft_del_line(&node->buf, i + 1);
		str[i] = '\0';
		(*line) = str;
		return (1);
	}
	return (ft_read(node, str, i, line));
}
