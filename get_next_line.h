/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/23 14:56:59 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/24 16:48:22 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include <stdlib.h>
# include <unistd.h>
# include "libft/libft.h"
# define BUFF_SIZE 1000

int					get_next_line(const int fd, char **line);
typedef struct		s_chain
{
	int				new_fd;
	char			buf[BUFF_SIZE];
	struct s_chain	*next;
}					t_chain;
#endif
