/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_transform.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 13:34:58 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 15:03:52 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_rotate(int keycode, int xyz[3])
{
	if (keycode == 49)
	{
		xyz[0] = -60;
		xyz[1] = 0;
		xyz[2] = -30;
	}
	if (keycode == 92)
		xyz[2] += 5;
	else if (keycode == 89)
		xyz[2] -= 5;
	else if (keycode == 88)
		xyz[1] += 5;
	else if (keycode == 86)
		xyz[1] -= 5;
	else if (keycode == 91)
		xyz[0] -= 5;
	else if (keycode == 87)
		xyz[0] += 5;
}

void	ft_move(int keycode, t_map *m)
{
	if (keycode == 123)
		m->shift_x -= 20;
	else if (keycode == 124)
		m->shift_x += 20;
	else if (keycode == 126)
		m->shift_y -= 20;
	else if (keycode == 125)
		m->shift_y += 20;
}

void	ft_zoom_z(int keycode, t_map *m)
{
	if (keycode == 0)
		m->k_z = m->k_z * 1.1;
	else if (keycode == 6)
		m->k_z = m->k_z / 1.1;
	if (keycode == 69)
		m->sc = m->sc * 1.2;
	else if (keycode == 78)
		m->sc = m->sc / 1.2;
}

void	ft_reset(int keycode, t_map *m)
{
	if (keycode == 49)
	{
		m->shift_x = 250;
		m->shift_y = 250;
		m->sc = 1;
		ft_make_z(m);
	}
}
