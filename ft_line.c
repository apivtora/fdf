/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_line.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 18:35:17 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 15:01:58 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		ft_fill(t_line *l, t_map2d *first, t_map2d *second)
{
	l->x0 = first->x;
	l->x1 = second->x;
	l->y0 = first->y;
	l->y1 = second->y;
	l->dx = abs(l->x1 - l->x0);
	l->sx = l->x0 < l->x1 ? 1 : -1;
	l->dy = abs(l->y1 - l->y0);
	l->sy = l->y0 < l->y1 ? 1 : -1;
	l->err = (l->dx > l->dy ? l->dx : -(l->dy)) / 2;
}

static void		ft_if_x(t_line *l)
{
	l->err = l->err - l->dy;
	l->x0 += l->sx;
}

static void		ft_if_y(t_line *l)
{
	l->err += l->dx;
	l->y0 += l->sy;
}

static void		ft_line_build(t_line l, t_map *m, t_color k_col, int *f_s)
{
	int k;
	int buf;
	int *nb;

	k = (l.dx > l.dy ? l.x0 - l.x1 : l.y0 - l.y1);
	buf = l.dx > l.dy ? l.x0 : l.y0;
	nb = l.dx > l.dy ? &(l.x0) : &(l.y0);
	ft_make_k(f_s[0], f_s[1], &k_col);
	if (k == 0)
		k = 1;
	while (1)
	{
		mlx_pixel_put(m->mlx, m->win, l.x0, l.y0, f_s[0] +
				(((k_col.r * (*nb - buf) / k) << 16) + ((k_col.g *
						(*nb - buf) / k) << 8) + k_col.b * (*nb - buf) / k));
		if ((l.x0 == l.x1 && l.y0 == l.y1) || l.x0 > 1300 ||
				l.y0 > 1300 || l.x0 < -1000 || l.y0 < -100)
			break ;
		l.e2 = l.err;
		if (l.e2 > -(l.dx))
			ft_if_x(&l);
		if (l.e2 < l.dy)
			ft_if_y(&l);
	}
}

void			ft_line(t_map2d *first, t_map2d *second, t_map *m)
{
	t_line	l;
	int		f_s[2];
	t_color	k_col;

	k_col.r = 0;
	ft_fill(&l, first, second);
	f_s[0] = first->c;
	f_s[1] = second->c;
	ft_line_build(l, m, k_col, f_s);
}
