/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 14:07:35 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 15:04:30 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	key(int keycode, t_map *m)
{
	static int i = 1;
	static int xyz[3] = {-60, 0, -30};

	ft_reset(keycode, m);
	ft_rotate(keycode, xyz);
	if (keycode == 53)
		exit(0);
	ft_move(keycode, m);
	if (keycode == 8)
	{
		if (!m->m2d[0][0]->alt_c)
			ft_change_color(m);
		ft_color_switch(m);
	}
	ft_zoom_z(keycode, m);
	if (keycode == 7)
		i = -i;
	ft_map2d_fill(m, xyz[0], xyz[1], xyz[2]);
	ft_build(m);
	if (i < 0)
		ft_build_x(m);
	ft_key_bind(keycode, m);
	return (0);
}

int	main(int argc, char **argv)
{
	t_map *m;

	m = ft_m_create();
	if (argc != 2)
	{
		write(1, "\x1b[31m", 5);
		write(1, "Error: ", 7);
		write(1, "\x1b[0m", 4);
		write(1, "wrong number of aguments\n", 26);
		return (0);
	}
	m->mass = ft_read(argv[1], m);
	ft_make_z(m);
	ft_map2d_fill(m, -60, 0, -30);
	m->mlx = mlx_init();
	m->win = mlx_new_window(m->mlx, 1200, 1000, "mlx 13");
	ft_build(m);
	ft_key_bind(0, m);
	mlx_hook(m->win, 2, 5, key, m);
	mlx_loop(m->mlx);
	free(m);
	return (0);
}
