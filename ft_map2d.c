/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map2d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 13:57:51 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 13:15:37 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_map2d	*ft_map2d_cell(void)
{
	t_map2d *m2d;

	m2d = (t_map2d*)malloc(sizeof(*m2d));
	m2d->x = 0;
	m2d->y = 0;
	m2d->c = 0xffffff;
	return (m2d);
}

t_map2d			***ft_map2d_create(t_map *m)
{
	int		i;
	int		j;
	t_map2d	***proj;

	proj = malloc(sizeof(*proj) * m->rows);
	j = 0;
	while (j < m->rows + 1)
	{
		i = 0;
		proj[j] = malloc(sizeof(**proj) * m->column + 1);
		while (i < m->column)
		{
			proj[j][i] = ft_map2d_cell();
			i++;
		}
		j++;
	}
	return (proj);
}

void			ft_map2d_fill(t_map *m, int o_x, int o_y, int o_z)
{
	int i;
	int j;
	int k;

	k = m->k_x / 3;
	j = 0;
	while (j < m->rows)
	{
		i = 0;
		while (i < m->column)
		{
			m->m2d[j][i]->x = m->c_x + m->shift_x + ((i * m->k_x - m->c_x)
					* m->sc * cos(COF * o_z) - (j * m->k_y - m->c_y)
					* m->sc * sin(COF * o_z)) * cos(COF * o_y) +
				m->mass[j][i] * m->k_z * m->sc * sin(COF * o_y);
			m->m2d[j][i]->y = m->c_y + m->shift_y + ((j * m->k_y - m->c_y)
					* m->sc * cos(COF * o_z) + (i * m->k_x - m->c_x) * m->sc *
					sin(COF * o_z)) * cos(COF * o_x) + (-((i * m->k_x - m->c_x)
					* m->sc * cos(COF * o_z) - (j * m->k_y - m->c_y) * m->sc *
					sin(COF * o_z)) * sin(COF * o_y) + m->mass[j][i] * m->k_z *
					m->sc * cos(COF * o_y)) * sin(COF * o_x);
			i++;
		}
		j++;
	}
}
