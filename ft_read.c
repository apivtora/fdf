/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 12:02:34 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 13:02:15 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_get_col(char *line)
{
	int i;

	i = 0;
	while (line[i] != 'x')
	{
		if (line[i] == '\0')
		{
			return (0xffffff);
		}
		i++;
	}
	return (ft_atoi_base(line + i + 1, 16));
}

static void	ft_params(t_map *m)
{
	m->k_x = 800 / m->column;
	m->k_y = 800 / m->rows;
	m->c_x = (m->k_x * (m->column - 1)) / 2;
	m->c_y = (m->k_y * (m->rows - 1)) / 2;
	m->shift_x = 250;
	m->shift_y = 250;
	m->sc = 1;
}

static int	**ft_make_mass(char *argv, t_map *m, int fd)
{
	int		**mass;
	char	*temp;
	char	**nbrs;
	int		j;
	int		i;

	mass = (int **)ft_memalloc(sizeof(*mass) * m->rows);
	fd = open(argv, O_RDONLY);
	j = 0;
	while (get_next_line(fd, &temp))
	{
		i = 0;
		mass[j] = ft_memalloc(sizeof(**mass) * m->column);
		nbrs = ft_strsplit(temp, ' ');
		while (nbrs[i])
		{
			mass[j][i] = ft_atoi(nbrs[i]);
			m->m2d[j][i]->c = ft_get_col(nbrs[i]);
			i++;
		}
		j++;
	}
	close(fd);
	return (mass);
}

int			**ft_read(char *argv, t_map *m)
{
	int		fd;
	char	*temp;
	int		i;

	i = 0;
	m->rows = 1;
	fd = open(argv, O_RDONLY);
	ft_error1(fd);
	get_next_line(fd, &temp);
	m->column = ft_make_col(temp);
	while (get_next_line(fd, &temp))
	{
		ft_check(temp, m);
		m->rows++;
	}
	close(fd);
	ft_params(m);
	m->m2d = ft_map2d_create(m);
	m->m2d[0][0]->alt_c = 0;
	return (ft_make_mass(argv, m, fd));
}
