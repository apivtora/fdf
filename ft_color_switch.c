/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color_switch.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 16:38:45 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 13:17:17 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_color_switch(t_map *m)
{
	int i;
	int j;
	int buf;

	j = 0;
	while (j < m->rows)
	{
		i = 0;
		while (i < m->column)
		{
			buf = m->m2d[j][i]->alt_c;
			m->m2d[j][i]->alt_c = m->m2d[j][i]->c;
			m->m2d[j][i]->c = buf;
			i++;
		}
		j++;
	}
}
