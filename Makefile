# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apivtora <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/27 14:54:23 by apivtora          #+#    #+#              #
#    Updated: 2017/02/27 14:57:19 by apivtora         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

FLAG = -Wall -Werror -Wextra -lmlx -framework OpenGL -framework AppKit

SRC = ft_errors.c get_next_line.c ft_key_bind.c	ft_line.c ft_m_create.c ft_make_col.c\
	ft_make_k.c ft_atoi_base.c ft_make_z.c ft_build.c ft_map2d.c ft_change_color.c\
	ft_min_max.c main.c ft_check.c ft_read.c ft_color_switch.c ft_transform.c
OBJ = ft_errors.o get_next_line.o ft_key_bind.o	ft_line.o ft_m_create.o ft_make_col.o\
	ft_make_k.o ft_atoi_base.o ft_make_z.o ft_build.o ft_map2d.o ft_change_color.o\
	ft_min_max.o main.o ft_check.o ft_read.o ft_color_switch.o ft_transform.o

HEADER = fdf.h

LIB = libft/libft.a

all : $(NAME)

$(NAME) : $(OBJ)
	make -C libft
	gcc  $(OBJ) $(FLAG) $(LIB) -o $(NAME)

%.o: %.c
	gcc -c -o $@ $< -Wall -Werror -Wextra 

clean:
	make clean -C ./libft
	rm -rf $(OBJ)

fclean: clean
	make fclean -C ./libft
	rm -rf $(NAME)

re: fclean all
	make re -C ./libft
