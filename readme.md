# FDF #
This is UNIT factory (school 42) studying graphical project that takes map of  
area(2d array of ints, see example)as agument(plain text file) and converts it  
to 3d image(actually 2.5d)- wire frame, which uses x and y pos of map numbers  
as X and Y coordinates and it's value as z, for example:  

> 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  
> 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  
> 0  0 10 10  0  0 10 10  0  0  0 10 10 10 10 10  0  0  0  
> 0  0 10 10  0  0 10 10  0  0  0  0  0  0  0 10 10  0  0  
> 0  0 10 10  0  0 10 10  0  0  0  0  0  0  0 10 10  0  0  
> 0  0 10 10 10 10 10 10  0  0  0  0 10 10 10 10  0  0  0  
> 0  0  0 10 10 10 10 10  0  0  0 10 10  0  0  0  0  0  0  
> 0  0  0  0  0  0 10 10  0  0  0 10 10  0  0  0  0  0  0  
> 0  0  0  0  0  0 10 10  0  0  0 10 10 10 10 10 10  0  0  
> 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  
> 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  

produces:

![example](https://bytebucket.org/apivtora/fdf/raw/685af4397e06b670cd26ad325cef1f35fd04ad3d/Screenshots/Example.jpg?token=f16d772415ad4674ffad2f6348445a88576b8605)

also there can be added colors in map in hex, using comma like:  
20,0xFF0000 15,0xFF0000 12 15,0xFF0000 17,0xFF0000  

Also you can rotate, zoom, change colors etc...(All -controls are displayed on output screen)  

##Some real output examples##  

![exmaple one](https://bytebucket.org/apivtora/fdf/raw/d5c72b94dc943425b5817efcbb5f71e8dd177a14/Screenshots/Screen%20Shot%202017-10-23%20at%205.47.37%20PM.png?token=090e003a3dfe97000e8634a89e0b31ec3f2c83f6)
![exmaple two](https://bytebucket.org/apivtora/fdf/raw/d5c72b94dc943425b5817efcbb5f71e8dd177a14/Screenshots/Screen%20Shot%202017-10-23%20at%205.49.54%20PM.png?token=91008f22204bdbc99ef75faf2dc1b7fb8a1dcfe1)
![example three](https://bytebucket.org/apivtora/fdf/raw/d5c72b94dc943425b5817efcbb5f71e8dd177a14/Screenshots/Screen%20Shot%202017-10-23%20at%205.50.23%20PM.png?token=8187cebd1c79fc98fbba6bb377d599f0a039b8e7)
![example four](https://bytebucket.org/apivtora/fdf/raw/d5c72b94dc943425b5817efcbb5f71e8dd177a14/Screenshots/Screen%20Shot%202017-10-23%20at%205.51.36%20PM.png?token=69e9f858624c4df495d83add61d5ccc037ded10a)

You can find some example map files is 'maps' folder 

### Project details ###

This project is written for macOS(OS X El Capitan)  
using MinilibX graphic library (https://github.com/abouvier/minilibx)  
and it is needed to run this project  
Use 'make' to install project  

If you are running Linux you need add -lm flag to compiling flags, as well as flags for MinilibX  
specified in its documentation. Also keycodes for Linux differ from macOS, co controls may differ  

### Disclaimer ###

This project is written according to 42's "The Norm" which specifies special rules for code like  
less then 25 lines in each function, 85 symbols in line, forbidden most of libraries and functions that wasnt  
written by you, also "for", "switch case", "goto" are forbidden etc...   
You can read more at (https://ncoden.fr/datas/42/norm.pdf)  
So to achieve some conditions sometimes code is modified to the point where it bacomes not really readble  

