/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_change_color.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 13:42:24 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 15:03:10 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_ctable(int c)
{
	int table[11];

	table[0] = 0x4169e1;
	table[1] = 0x87ceeb;
	table[2] = 0xf5fffa;
	table[3] = 0xcce698;
	table[4] = 0xf0e68c;
	table[5] = 0xfdde81;
	table[6] = 0xffa500;
	table[7] = 0xcd853f;
	table[8] = 0x8b4513;
	table[9] = 0x8f0a00;
	table[10] = 0xff0000;
	return (table[c]);
}

static void	ft_change_color_p2(t_map *m, int max, int min)
{
	int dif;
	int i;
	int j;

	dif = max - min;
	if (dif < 9)
		dif = 9;
	j = 0;
	while (j < m->rows)
	{
		i = 0;
		while (i < m->column)
		{
			m->m2d[j][i]->alt_c = ft_ctable((m->mass[j][i] - min) / (dif / 9));
			i++;
		}
		j++;
	}
}

void		ft_change_color(t_map *m)
{
	int *min_max;

	min_max = ft_min_max(m);
	ft_change_color_p2(m, min_max[1], min_max[0]);
}
