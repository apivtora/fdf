/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_m_create.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 14:26:52 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 10:16:12 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_map	*ft_m_create(void)
{
	t_map *m;

	m = (t_map*)malloc(sizeof(*m) + 50);
	if (!m)
	{
		exit(0);
	}
	m->mlx = NULL;
	m->win = NULL;
	m->column = 0;
	m->mlx = NULL;
	m->win = NULL;
	m->mass = NULL;
	m->rows = 0;
	m->m2d = ft_map2d_create(m);
	return (m);
}
