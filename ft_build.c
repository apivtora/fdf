/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_build.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apivtora <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 16:40:03 by apivtora          #+#    #+#             */
/*   Updated: 2017/02/27 12:34:30 by apivtora         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_build_x(t_map *m)
{
	int i;
	int j;

	j = 0;
	while (j < m->rows - 1)
	{
		i = 0;
		while (i < m->column - 1)
		{
			ft_line(m->m2d[j][i], m->m2d[j + 1][i + 1], m);
			ft_line(m->m2d[j][i + 1], m->m2d[j + 1][i], m);
			i++;
		}
		j++;
	}
}

void	ft_build(t_map *m)
{
	int i;
	int j;

	j = 0;
	mlx_clear_window(m->mlx, m->win);
	while (j < m->rows)
	{
		i = 0;
		while (i < m->column)
		{
			if (i < m->column - 1)
				ft_line(m->m2d[j][i], m->m2d[j][i + 1], m);
			if (j < m->rows - 1)
				ft_line(m->m2d[j][i], m->m2d[j + 1][i], m);
			i++;
		}
		j++;
	}
}
